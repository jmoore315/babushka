dep "VirtualBox.installer" do
  source "http://dlc.sun.com.edgesuite.net/virtualbox/4.2.6/VirtualBox-4.2.6-82870-OSX.dmg"
  met? { "/Applications/VirtualBox.app".p.exists? }
end

dep "devops" do
  requires "VirtualBox.installer"
end
