def home
  ENV["HOME"]
end

def bash_profile
  File.join(home, ".bash_profile")
end

dep "cask" do
  met? {
    "/usr/local/Cellar/cask".p.exists?
  }
  meet {
    shell "brew install caskroom/cask/brew-cask"  
  }
end

dep "environment" do
  requires "cask"
end
